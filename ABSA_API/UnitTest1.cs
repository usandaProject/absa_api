using ABSA_API;
using NUnit.Framework;

namespace Tests
{



    public class Tests
    {

        API api;
        [SetUp]
        public void Setup()
        {
            api = new API();
        }

        [Test]
        public void PrintListOfDogs()
        {

            api.GetListOfDogs();
            api.ListofPrintDog();
            Assert.IsTrue(api.Statu200());
             
        }
        [Test]
        public void RetrieverFound()
        {
            api.GetListOfDogs();
            Assert.IsTrue(api.Retriever(), "Retriver is within List");
        }

        [Test]
        public void GetRetrieverSubBreeds()
        {

            api.GetListOfDogs();
            api.Serialize();
            api.GetRetrieverSubBreeds();
            Assert.IsTrue(api.Statu200());
        }

        [Test]
        public void Get_RandomImages()
        {

            api.Get_RandomImages();
            Assert.IsTrue(api.Statu200());

        }



    }
}